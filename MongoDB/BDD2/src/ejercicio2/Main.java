package ejercicio2;


import java.util.ArrayList;
import java.util.Arrays;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
public class Main {

	public static void main(String[] args) {
		
		
		ArrayList<trabajadores>Trabajadores = new ArrayList<trabajadores>();

	   Trabajadores.add(new trabajadores("Roberto", "Perez Gutierrez",   39, new ArrayList<String>(Arrays.asList("Programador web")),                       new ArrayList<String>(Arrays.asList("Python")), false));
	   Trabajadores.add(new trabajadores("Juan", "Sanchez Gonz�lez",     33, new ArrayList<String>(Arrays.asList("Consultor tecnologico")),                 new ArrayList<String>(Arrays.asList("Java" ,"Javascript","Scala")), true));
	   Trabajadores.add(new trabajadores("Sergio", "Moreno Claro", 	   34,new ArrayList<String>(Arrays.asList("Dise�ador de sistemas informaticos")),     new ArrayList<String>(Arrays.asList("C", "Go","C++")), true));
	   Trabajadores.add(new trabajadores("Lucas", "Ruiz Serrano", 	   29,new ArrayList<String>(Arrays.asList("Responsable de entornos de seguridad")),   new ArrayList<String>(Arrays.asList("Python", "Ruby")), true));
	   Trabajadores.add(new trabajadores("Juan", "Tortajada Gutierrez", 43,new ArrayList<String>(Arrays.asList("Administradpr de sistemas y redes")),      new ArrayList<String>(Arrays.asList("Java")), true));
	   Trabajadores.add(new trabajadores("Lucia", " Saez Lopez",         32,new ArrayList<String>(Arrays.asList("Analista de sistemas informaticos")),      new ArrayList<String>(Arrays.asList("Net","C++","PhP","Scala")), false));
	   Trabajadores.add(new trabajadores("Pilar", "Torres Cuenca",       37,new ArrayList<String>(Arrays.asList("Creadora de videojuegos y aplicaciones")), new ArrayList<String>(Arrays.asList("C++","C#","Java","Python")), false));
	   Trabajadores.add(new trabajadores("Margarita", "Romero Gil",      32,new ArrayList<String>(Arrays.asList( "Creadora de aplicaciones moviles")),      new ArrayList<String>(Arrays.asList("Java")), true));
	   Trabajadores.add(new trabajadores("Lola", "Mento Rubio ",         21,new ArrayList<String>(Arrays.asList("Programador web")),                       new ArrayList<String>(Arrays.asList("Javascript","Python","Java","C++")), true));
	    
	    try {
	        
	        MongoClient mongoClient = new MongoClient("localhost", 27017);
	        @SuppressWarnings("deprecation")
			DB database = mongoClient.getDB("Empresa");
	        DBCollection collection = database.getCollection("Trabajadores");

	       for (trabajadores tra :Trabajadores) {
	            collection.insert(tra.toDBObjectTrabajadores());
	       }
	          // Esto busca todos los documentos de la collecion y los imprime
	          
	          DBCursor cursor = collection.find();
	          try {
	            while (cursor.hasNext()) {
	              System.out.println(cursor.next().toString());
	            }
	          } finally {
	            cursor.close();
	          }
	          
	          // Cuantos documentos hay en la base de datos
	          int numDocumentos = (int) collection.getCount();
	          System.out.println(numDocumentos + "\n");
	          /**
	          //DELETE 
	          DBObject findDoc = new BasicDBObject("trabajoOnline", true);
	          DBObject findDoc2 = new BasicDBObject("trabajoOnline",false);
	          collection.remove(findDoc);
	          collection.remove(findDoc2);
	          
	          /**
	            **/
	          /**
	           
	         //READ  Hacemos una Query con condiciones y lo pasamos a  objeto Java
	         
	          System.out.println("\nTrabajadores que son programador web");
	          DBObject query = new BasicDBObject("trabajo", new BasicDBObject("$regex", "Programador web"));
	          cursor = collection.find(query);
	          try {
	            while (cursor.hasNext()) {
	             trabajadores Trabajadores1 = new trabajadores((BasicDBObject) cursor.next());
	              System.out.println(Trabajadores1.toString());
	              
	              
	            }
	          } finally {
	            cursor.close();
	          }
	          **/
			
			/**
	         System.out.println("\nTrabajadores que no son programador web");
	          DBObject query = new BasicDBObject("trabajo", new BasicDBObject("$ne", "Programador web"));
	          cursor = collection.find(query);
	          try {
	            while (cursor.hasNext()) {
	             trabajadores Trabajadores1 = new trabajadores((BasicDBObject) cursor.next());
	              System.out.println(Trabajadores1.toString());
	              
	              
	            }
	          } finally {
	            cursor.close();
	          }
	          /**
	          */
	          

					
	      /**
	          
	          // UPDATE  Actualizamos la edad de los jugadores.
	        
	          DBObject find = new BasicDBObject("edad", new BasicDBObject("$eq", 21));
	          DBObject updated = new BasicDBObject().append("$inc", new BasicDBObject().append("edad", 10));
	          collection.update(find, updated, false, true);
			/**
	       */    
	          
	          
	          
	          
	        //  Cerrar la conexion
	          mongoClient.close();

	        } catch (Exception ex) {
	          System.out.println("Error al conectar al servidor de Mongo: " + ex.getMessage());
	          
	        }
	      }
	    
	
	
	}
	


