package ejercicio2;

import java.util.ArrayList;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class trabajadores {
	private String nombre;
	private String apellidos;
	private Integer edad;
	private ArrayList<String> trabajo;
	private ArrayList<String> lenguaje;
	private Boolean trabajoOnline;

	public trabajadores() {
	}

	public trabajadores(String nombre, String apellidos, Integer edad, ArrayList<String> trabajo ,ArrayList<String> lenguaje, Boolean trabajoOnline) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.trabajo = trabajo;
		this.lenguaje = lenguaje;
		this.trabajoOnline = trabajoOnline;
		
	}

	// OBJETO MONGODB A JAVA

	public trabajadores(BasicDBObject dBObjectTrabajadores) {
		this.nombre = dBObjectTrabajadores.getString("nombre");
		this.apellidos = dBObjectTrabajadores.getString("apellidos");
		this.edad = dBObjectTrabajadores.getInt("edad");
		
		
		BasicDBList listtrabajo = (BasicDBList) dBObjectTrabajadores.get("trabajo");
		this.trabajo = new ArrayList<String>();
		for (Object trabajo : listtrabajo) {
			this.trabajo.add(trabajo.toString());
		}

		
		BasicDBList listlenguajees = (BasicDBList) dBObjectTrabajadores.get("lenguaje");
		this.lenguaje = new ArrayList<String>();
		for (Object lenguajes : listlenguajees) {
			this.lenguaje.add(lenguajes.toString());
		}

		this.trabajoOnline = dBObjectTrabajadores.getBoolean("trabajoOnline");
	}

	//OBJETO JAVA A MONGODB
	public DBObject toDBObjectTrabajadores() {
		
		BasicDBObject dBObjectTrabajadores = new BasicDBObject();

		dBObjectTrabajadores.append("nombre", this.getNombre());
		dBObjectTrabajadores.append("apellidos", this.getApellidos());
		dBObjectTrabajadores.append("edad", this.getEdad());
		dBObjectTrabajadores.append("trabajo", this.gettrabajo());
		dBObjectTrabajadores.append("lenguaje", this.getlenguaje());
		dBObjectTrabajadores.append("trabajoOnline", this.gettrabajoOnline());	
		
		return dBObjectTrabajadores;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public ArrayList<String> gettrabajo() {
		return trabajo;
	}

	public void settrabajo(ArrayList<String> trabajo) {
		this.trabajo = trabajo;
	}
	public ArrayList<String> getlenguaje() {
		return lenguaje;
	}

	public void setlenguaje(ArrayList<String> lenguaje) {
		this.lenguaje = lenguaje;
	}

	public Boolean gettrabajoOnline() {
		return trabajoOnline;
	}

	public void settrabajoOnline(Boolean trabajoOnline) {
		this.trabajoOnline = trabajoOnline;
	}

	@Override
	public String toString() {
		return "Nombre: " + this.getNombre() + " " + this.getApellidos() + " / Edad: " + this.edad +" / Trabajo: " + this.trabajo.toString() + " / Lenguajes: " + this.lenguaje.toString() + " / Trabajo online: " + this.trabajoOnline;
	}
}
