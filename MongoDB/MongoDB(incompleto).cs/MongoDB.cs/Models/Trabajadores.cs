﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;



namespace MongoDB.cs.Models
{
    class Trabajadores
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        [BsonElement("nombre")]
        public string Name{get; set;}
       
        [BsonElement("apellido")]
        public string Surname{ get; set; }
        
        [BsonElement("edad")]
        public int Age { get; set; }
        
        [BsonElement("trabajo")]
        public string Job { get; set; }
        
        [BsonElement("lenguaje")]
        public string Language { get; set; }
       
        [BsonElement("trabajoOnline")]
        public Boolean OnlineJob { get; set; }

        
    }
}
